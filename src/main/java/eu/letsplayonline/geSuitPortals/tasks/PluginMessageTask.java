package eu.letsplayonline.geSuitPortals.tasks;

import eu.letsplayonline.geSuitPortals.geSuitPortals;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.ByteArrayOutputStream;

public class PluginMessageTask extends BukkitRunnable {

    private final ByteArrayOutputStream bytes;

    public PluginMessageTask(ByteArrayOutputStream bytes) {
        this.bytes = bytes;
    }

    public void run() {
        Bukkit.getOnlinePlayers().iterator().next().sendPluginMessage(
                geSuitPortals.INSTANCE,
                "geSuitPortals",
                bytes.toByteArray());

    }

}